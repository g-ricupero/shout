\include "articulate.ly"
\include "../include/global.ly"
\include "../include/harmony.ly"
\include "../include/outline.ly"
\include "../include/tenorA.ly"

\score {
	\unfoldRepeats \articulate
	\new StaffGroup <<
		\new ChordNames {
			\harmony
		}
		\new Staff <<
			\set Staff.midiInstrument = "tenor sax"
			\global \outline \tenorA
		>>
	>>
	\midi {}
}
