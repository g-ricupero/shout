\version "2.18.2"

\paper {
	#(set-paper-size "a4")          % paper size
	% ragged-last-bottom = ##f      % fill the page till bottom
	% page-count = 1                % force to render just in 1 page
	% between-system-padding = 0    % empty padding between lines
}

NoChords = {
	\override Score.MetronomeMark #'padding = #4
}
WithChords = {
	\override Score.MetronomeMark #'padding = #8
}

\header {
	title = \markup \center-align {
		\override #'(font-name . "Purisa")
		\fontsize #4 \bold
		"Shout"
	}
	subsubtitle = \markup \center-align {
		\override #'(font-name . "Arial")
		"Animal House"
	}
	composer=\markup \center-align {
		\override #'(font-name . "Arial")
		"The Isley Brothers"
	}
	opus = \markup \tiny {
		\override #'(font-name . "Arial")
		"(1959)"
	}
	tagline = \markup {
		\override #'(font-name . "Arial")
		\tiny \column {
			\fill-line { "Transcription" \italic { "Giuseppe Ricupero" } }
			\fill-line { "Updated" \italic { "04-04-2017 20.34" } }
		}
	}
}

global = {
	\time 4/4
	\key ebes \major
	\tempo 4 = 146
	\set Score.skipBars = ##t
	\set Score.countPercentRepeats = ##t
	\set Staff.hairpinToBarline = ##f
}
