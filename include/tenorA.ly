tenorA = \relative c' {
	\set Staff.instrumentName = "Tenor A"

	%% A %%
	g1*4
	%% B %%
	c1*4
}
