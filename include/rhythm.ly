rhythm = {
	\improvisationOn
	%% A %%
	\repeat percent 4 { c4 c4 c4 c4 }
	%% B %%
	\repeat percent 4 { c4 c4 c4 c4 }
	\bar "|."
}

rhythmPO = {
	\set Staff.instrumentName = "Rhythm Section"
}

harmonyR = \chordmode {
	%% A %%
	\repeat percent 4 { c1:7 }
	%% B %%
	\repeat percent 4 { f1:7 }
}
