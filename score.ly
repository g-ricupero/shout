\include "include/global.ly"
\include "include/harmony.ly"
\include "include/outline.ly"
\include "include/soprano.ly"
\include "include/alto.ly"
\include "include/tenorA.ly"
\include "include/tenorB.ly"
\include "include/bass.ly"
\include "include/ebass.ly"

\header {
	instrument = \markup \italic \normalsize {
		\override #'(font-name . "Arial")
		"Score"
	}
}

\score {
	\new StaffGroup <<
		\new ChordNames {
			\set chordChanges = ##t
			\harmony
		}
		\new Staff <<
			\clef treble
			\WithChords \global \outline \soprano
		>>
		\transpose c c' \new Staff <<
			\clef treble
			\WithChords \global \outline \alto
		>>
		\transpose c c' \new Staff <<
			\clef treble
			\WithChords \global \outline \tenorA
		>>
		\new Staff <<
			\clef bass
			\WithChords \global \outline \tenorB
		>>
		\transpose c c'' \new Staff <<
			\clef treble
			\WithChords \global \outline \bass
		>>
		% \transpose c c' \new Staff <<
		% 	\clef bass
		% 	\WithChords \global \outline \ebass
		% >>
	>>
	\layout {}
}
